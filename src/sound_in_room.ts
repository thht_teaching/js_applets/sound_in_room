import { SVG, Rect} from "@svgdotjs/svg.js";
import '@svgdotjs/svg.draggable.js';
import { Loudspeaker} from "./lib/loudspeaker";
import {Listener} from "./lib/listener";
// @ts-ignore
import metal from './assets/ogg/metal.ogg';
// @ts-ignore
import trap from './assets/ogg/trap.ogg';
// @ts-ignore
import classical from './assets/ogg/classical.ogg';

export default async function(container_element: HTMLElement, ctx: AudioContext, width=500, height=300) {
    const svg = SVG().addTo(container_element).size(width, height).viewbox(0, 0, 25, 20);
    const ls = {
        metal: await Loudspeaker.get_loudspeaker(metal, ctx),
        trap: await Loudspeaker.get_loudspeaker(trap, ctx),
        classical: await Loudspeaker.get_loudspeaker(classical, ctx)
    }

    for (let lsKey in ls) {
        ls[lsKey].addTo(svg);
        ls[lsKey].size(4)

    }
    const listener = new Listener();
    listener.addTo(svg);
    listener.size(4);
    set_listener_position(0, 0, 0);
    set_listener_orientation(0, 0, -1, 0, 1, 0);
    listener.on('dragmove', (event) => {
        set_listener_position(<number>listener.x(), <number>listener.y(), 0);
    });

    function set_listener_position(x: number, y: number, z: number) {
        if (typeof ctx.listener.positionX !== 'undefined') {
            ctx.listener.positionX.value = x;
            ctx.listener.positionY.value = y;
            ctx.listener.positionZ.value = z;
        } else {
            ctx.listener.setPosition(x, y, z);
        }
    }

    function set_listener_orientation(forwardX: number, forwardY: number, forwardZ: number, upX: number, upY: number, upZ: number) {
        if (typeof ctx.listener.forwardX !== 'undefined') {
            ctx.listener.forwardX.setTargetAtTime(forwardX, ctx.currentTime, 0.1);
            ctx.listener.forwardY.setTargetAtTime(forwardY, ctx.currentTime, 0.1);
            ctx.listener.forwardZ.setTargetAtTime(forwardZ, ctx.currentTime, 0.1);
            ctx.listener.upX.setTargetAtTime(upX, ctx.currentTime, 0.1);
            ctx.listener.upY.setTargetAtTime(upY, ctx.currentTime, 0.1);
            ctx.listener.upZ.setTargetAtTime(upZ, ctx.currentTime, 0.1);
        } else {
            ctx.listener.setOrientation(forwardX, forwardY, forwardZ, upX, upY, upZ);
        }
    }
}
