import {Svg} from "@svgdotjs/svg.js";


class SoundEmitter extends Svg {
    is_active = false;

    constructor() {
        super();
        this.on('click', () => this.handle_click());
        this.set_filter();
    }

    set_filter() {
        if (this.is_active) {
            this.css('filter', 'grayscale(0)');
        } else {
            this.css('filter', 'grayscale(1)');
        }
    }

    handle_click() {
        this.is_active = !this.is_active;
        this.set_filter();
    }
}

export { SoundEmitter }
