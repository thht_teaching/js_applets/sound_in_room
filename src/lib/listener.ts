import { SVG, Svg } from '@svgdotjs/svg.js';
import '@svgdotjs/svg.draggable.js';
// @ts-ignore
import this_svg from '../assets/svg/listener.svg';

class Listener extends Svg {
    constructor() {
        super();
        this.svg(this_svg);
        this.draggable(true);
    }
}

export { Listener };
