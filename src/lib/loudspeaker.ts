import { SVG, Svg } from '@svgdotjs/svg.js';
import '@svgdotjs/svg.draggable.js';
// @ts-ignore
import this_svg from '../assets/svg/loudspeaker.svg';
import {SoundEmitter} from "./sound_emitter";

class Loudspeaker extends SoundEmitter {
    private constructor(ctx: AudioContext) {
        super();
        this.ctx = ctx;
        this.svg(this_svg);
        this.draggable(true);
        this.on('dragmove', (evt) => this.handle_drag(evt));
    }

    static async get_loudspeaker(sound_url: URL, ctx: AudioContext) {
        const obj = new Loudspeaker(ctx);
        const response = await fetch(sound_url);
        const tmp_data = await response.arrayBuffer();
        obj.sound_buffer = await ctx.decodeAudioData(tmp_data);
        return obj;
    }

    handle_click() {
        if (this.is_dragging) {
            this.is_dragging = false;
            return;
        }
        super.handle_click();
        if (this.is_active) {
            this.sound_buffersource = this.ctx.createBufferSource();
            this.sound_buffersource.buffer = this.sound_buffer;
            this.sound_buffersource.loop = true;
            this.panner = this.ctx.createPanner();
            this.panner.coneInnerAngle = 360;
            this.panner.coneOuterAngle = 360;
            this.panner.coneOuterGain = 0;
            this.panner.distanceModel = 'inverse';
            this.panner.maxDistance = 1000;
            this.panner.panningModel = 'HRTF';
            this.panner.refDistance = 1;
            this.panner.rolloffFactor = 1;
            this.sound_buffersource.connect(this.panner);
            this.panner.connect(this.ctx.destination);
            this.ctx.destination.channelCount = 1;
            this.sound_buffersource.start();
            this.panner.positionX.value = <number>this.x();
            this.panner.positionY.value = <number>this.y();
        } else {
            this.sound_buffersource.stop();
        }
    }

    handle_drag(event) {
        if(event.detail.event.type === 'mousemove' || event.detail.event.type === 'touchmove') {
            this.is_dragging = true;
            this.panner.positionX.value = <number>this.x();
            this.panner.positionY.value = <number>this.y();
        }
    }

    sound_buffer: AudioBuffer;
    sound_buffersource: AudioBufferSourceNode;
    ctx: AudioContext;
    panner: PannerNode;
    is_dragging = false;
}

export { Loudspeaker };
