import * as esbuild from 'esbuild';

const paramaters = {
    entryPoints: ['src/sound_in_room.ts'],
    outfile: 'dist/sound_in_room.min.js',
    format: 'esm',
    target: ['chrome60'],
    bundle: true,
    logLevel: 'info',
    loader: {
        '.svg': 'text',
        '.ogg': 'dataurl'
    }
}

if (process.argv[2] === 'dev' || process.argv[2] === 'dev_server') {
    paramaters['minify'] = false;
    paramaters['sourcemap'] = true;
} else {
    paramaters['minify'] = true;
    paramaters['sourcemap'] = false;
}

if (process.argv[2] === 'dev_server') {
    const ctx = await esbuild.context(paramaters);
    await ctx.serve({servedir: '.'})
} else {
    await esbuild.build(paramaters);
}
